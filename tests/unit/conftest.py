import os
import pytest
import responses
import sys
import uuid

from typing import Tuple
from urllib.parse import urlparse


try:
    sys.path.insert(0, os.environ["MODULE_ROOT"])
except KeyError:
    pass


@pytest.fixture
def minio_server(s3_server):
    server_url = s3_server.boto_endpoint_url
    return urlparse(server_url).netloc


@pytest.fixture
def minio_bucket_with_files(s3_bucket):
    bucket = s3_bucket.name
    object = s3_bucket.client.Object(bucket, "test")
    object.put(Body="test")
    return bucket


@pytest.fixture
def single_instance():
    echo_vars = "hello"
    return {
        "run-cmd": f'echo "{echo_vars}"',
        "project": "A Test Project",
        "run_id": 1,
        "stage": "tox_tests",
    }


@pytest.fixture
def threaded_instance():
    thread_id = uuid.uuid4()
    echo_vars = "hello"
    return {
        "thread_id": thread_id,
        "parent_id": thread_id,
        "run-cmd": f'echo "{echo_vars}"',
        "project": "A Test project",
        "run_id": 2,
        "stage": "tox_tests",
    }


@pytest.fixture
def common_headers(config, request_ctx):
    config["endpoint"] = request_ctx.request.host_url
    return {"Content-Type": "application/json", "X-Forwarded-Host": config["endpoint"]}


@pytest.fixture
def start_task_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, f"{config['gman']['url']}/task"),
        {
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
                "caller": "test_case_create_1",
                "task_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
            }
        },
    )


@pytest.fixture
def task_creation_response(config) -> Tuple[Tuple, dict]:
    return (
        (
            responses.PUT,
            f"{config['gman']['url']}/task/9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
        ),
        {
            "timestamp": "2019-07-17T12:10:32.952267+00:00",
            "event_id": "9038675b-66ad-4d8f-9c38-a3fa5a93db8c",
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
                "caller": "test_case_create_1",
                "task_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
            },
            "return_code": None,
            "status": "started",
            "message": " task creation body",
        },
    )


@pytest.fixture
def task_artifact_upload_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, f"{config['gman']['url']}/artifact"),
        {
            "status": "unknown",
            "uri": "https://someminio.example.com/art1",
            "artifact_id": "884053a3-277b-45e4-9813-fc61c07a2cd6",
            "type": "artifact",
            "sri": "sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=",
            "task": {
                "task_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
                "caller": "test_case_create_1",
                "project": "gman_test_data",
                "thread_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
                "run_id": "create_1",
            },
            "event_id": "a48efe28-db9e-4330-93c4-5f480b2bef71",
        },
    )
