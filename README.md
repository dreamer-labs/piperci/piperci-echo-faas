# PiperCI Echo Function

## Table of Contents

* [Getting Started](#getting-started)
* [Testing](#testing)
* [Testing Prerequisites](#testing-prerequisites)
* [Deploying to OpenFaaS Manually](#deploying-to-openfaas-manually)
* [Contributing](#contributing)
* [License](#license)

## Getting Started

It is recommended that you test and develop this function using the `tox -e unittest` tests which simulate an OpenFaaS environment.

To create a development OpenFaaS environment follow the [PiperCI Installation Guide](https://piperci.dreamer-labs.net/piperci-installer/README.html)

### Testing

```bash
tox -e lint,unittest
```

The scripts in tools/scripts manage installation of tooling needed to run tests and simulate a working OpenFaaS PiperCI environment so you can get down to the business of developing your function. As mentioned earlier use the entry point `tox -e unittest` for all your FaaS functions which will download and install the template and extract the components into a tox build environment and then run the tests.

You may also activate and use the tox virtualenv while developing your function `source .tox/unittest/bin/activate`

#### Testing Prerequisites

Requirements

* tox
* bash
* python3.7
* pip
* anything in `function_name/requirements.txt`

### Deploying to OpenFaaS Manually

To deploy this function to OpenFaaS do the following after authentication:

```
git clone https://gitlab.com/dreamer-labs/piperci/piperci-echo-faas.git
cd piperci-echo-faas
faas-cli template pull https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates
faas build
faas deploy
```

To validate that your function installed correctly you can run the following:

```
faas ls
```

## Contributing

Please read [CONTRIBUTING](https://gitlab.com/dreamer-labs/piperci/piperci-boilerplates/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting merge requests to us.

## License

[MIT](https://piperci.dreamer-labs.net/project-info/LICENSE.html)
